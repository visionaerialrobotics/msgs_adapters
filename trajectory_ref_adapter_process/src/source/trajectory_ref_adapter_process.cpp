/*!*******************************************************************************************
 *  \file       trajectory_ref_adapter_process.cpp
 *  \brief      TrajectoryRefAdapter implementation file.
 *  \details    This file implements the TrajectoryRefAdapter class.
 *  \authors    Daniel Del Olmo.
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/trajectory_ref_adapter_process.h"

TrajectoryRefAdapter::TrajectoryRefAdapter()
{
}

TrajectoryRefAdapter::~TrajectoryRefAdapter()
{
}

/*DroneProcess*/
void TrajectoryRefAdapter::ownSetUp()
{
  node_handle.param<std::string>("drone_id", drone_id, "1");
  node_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  node_handle.param<std::string>("drone_trajectory_ref_command", drone_trajectory_ref_str, "droneTrajectoryRefCommand");
  node_handle.param<std::string>("motion_reference_trajectory", motion_reference_trajectory_str,
                                 "motion_reference/trajectory");
}

void TrajectoryRefAdapter::ownStart()
{
  motion_reference_trajectory_pub = node_handle.advertise<aerostack_msgs::Trajectory>(
      '/' + drone_id_namespace + '/' + motion_reference_trajectory_str, 1);

  drone_trajectory_ref_sub = node_handle.subscribe('/' + drone_id_namespace + '/' + drone_trajectory_ref_str, 1,
                                                   &TrajectoryRefAdapter::trajectoryReferenceCallback, this);
}

void TrajectoryRefAdapter::ownRun()
{
}

void TrajectoryRefAdapter::ownStop()
{
  motion_reference_trajectory_pub.shutdown();
  drone_trajectory_ref_sub.shutdown();
}

void TrajectoryRefAdapter::trajectoryReferenceCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand& msg)
{
  std::vector<geometry_msgs::Point32> points;
  geometry_msgs::Polygon polygon_msgs;

  droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type trajectory = msg.droneTrajectory;

  for (int i = 0; i < trajectory.size(); i++)
  {
    geometry_msgs::Point32 point;
    point.x = trajectory[i].x;
    point.y = trajectory[i].y;
    point.z = trajectory[i].z;

    points.push_back(point);
  }
  polygon_msgs.points = points;
  motion_reference_trajectory_msg.Trajectory = polygon_msgs;
  motion_reference_trajectory_msg.is_periodic = msg.is_periodic;
  motion_reference_trajectory_msg.initial_checkpoint = msg.initial_checkpoint;

  motion_reference_trajectory_pub.publish(motion_reference_trajectory_msg);
}
