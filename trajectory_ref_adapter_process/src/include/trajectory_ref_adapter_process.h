/*!*********************************************************************************
 *  \file       trajectory_ref_adapter_process.h
 *  \brief      TrajectoryRefAdapter definition file.
 *  \details    This file contains the TrajectoryRefAdapter declaration.
 *              To obtain more information about it's definition consult
 *              the trajectory_ref_adapter_process.cpp file.
 *  \authors    Abraham Carrera, Daniel del Olmo.
 *  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
 *              All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef TRAJECTORY_REF_ADAPTER_PROCESS_H
#define TRAJECTORY_REF_ADAPTER_PROCESS_H

/*System*/
#include <string>
#include <ros/ros.h>

/*Messages*/
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/dronePositionRefCommand.h>
/*Aerostack*/
#include <robot_process.h>
#include <aerostack_msgs/Trajectory.h>
#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/Point32.h>

/*Class definition*/
class TrajectoryRefAdapter : public RobotProcess
{
public:
  TrajectoryRefAdapter();
  ~TrajectoryRefAdapter();

private:
  ros::NodeHandle node_handle;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string drone_trajectory_ref_str;
  std::string motion_reference_trajectory_str;

  ros::Publisher motion_reference_trajectory_pub;
  ros::Subscriber drone_trajectory_ref_sub;

  aerostack_msgs::Trajectory motion_reference_trajectory_msg;

  void trajectoryReferenceCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand&);

private:
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
};

#endif
