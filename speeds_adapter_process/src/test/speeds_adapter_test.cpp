/************************************************************************
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include <gtest/gtest.h>
#include "../include/speeds_adapter_process.h"
#include <cstdio>
#include <iostream>

/*Parameters*/
ros::NodeHandle* nh;
class TwistListener
{
public:
  ros::NodeHandle nh;
  ros::Subscriber trajectory_controller_geometric_speed_sub;

  bool trajectory_controller_messageReceived;

  TwistListener()
  {
    trajectory_controller_messageReceived = false;
  }

  void start()
  {
    trajectory_controller_geometric_speed_sub =
        nh.subscribe("/drone1/trajectoryControllerTwistReferencesRebroadcast", 1,
                     &TwistListener::trajectoryControllerSpeedCallback, this);
  }

  void trajectoryControllerSpeedCallback(const geometry_msgs::TwistStamped& message)
  {
    trajectory_controller_messageReceived = true;
  }
};

class SpeedsRefListener
{
public:
  ros::NodeHandle nh;
  std::string motion_reference_speeds_str;
  std::string drone_id;
  std::string drone_id_namespace;
  ros::Subscriber motion_reference_speeds_sub;

  bool motion_reference_speeds_msgReceived;

  SpeedsRefListener()
  {
    motion_reference_speeds_msgReceived = false;
  }

  void start()
  {
    nh.param<std::string>("drone_id", drone_id, "1");
    nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
    nh.param<std::string>("motion_reference_speeds", motion_reference_speeds_str, "motion_reference/speeds");
    motion_reference_speeds_sub = nh.subscribe('/' + drone_id_namespace + '/' + motion_reference_speeds_str, 1,
                                               &SpeedsRefListener::motionReferenceSpeedsCallback, this);
  }

  void motionReferenceSpeedsCallback(const geometry_msgs::TwistStamped& message)
  {
    motion_reference_speeds_msgReceived = true;
  }
};

TEST(AdaptersTests, speedsRebroadcastAdapterTests)
{
  ros::NodeHandle nh;
  TwistListener tL;
  SpeedsAdapter twist_adapter;

  std::string estimated_speed_str;
  std::string trajectory_controller_speed_str;

  std::string drone_id;
  std::string drone_id_namespace;

  ros::Publisher trajectory_controller_pose_pub;

  droneMsgsROS::droneSpeeds speed_msg;

  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  nh.param<std::string>("trajectoryControllerSpeedReferencesRebroadcast", trajectory_controller_speed_str,
                        "trajectoryControllerSpeedReferencesRebroadcast");

  trajectory_controller_pose_pub =
      nh.advertise<droneMsgsROS::droneSpeeds>('/' + drone_id_namespace + '/' + trajectory_controller_speed_str, 1);

  speed_msg.dx = 1;
  speed_msg.dy = 1;
  speed_msg.dz = 1;
  speed_msg.dyaw = 1;
  speed_msg.dpitch = 1;
  speed_msg.droll = 1;

  twist_adapter.setUp();
  twist_adapter.start();

  tL.start();

  ros::Rate rate(30);

  twist_adapter.run();
  trajectory_controller_pose_pub.publish(speed_msg);

  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();

  EXPECT_TRUE(tL.trajectory_controller_messageReceived);
}

TEST(adaptersTests, speedsRefAdapterTests)
{
  ros::NodeHandle nh;
  SpeedsRefListener pL;
  SpeedsAdapter speeds_reference_adapter;

  std::string speeds_reference_str;
  std::string drone_id;
  std::string drone_id_namespace;

  ros::Publisher speeds_reference_pub;

  droneMsgsROS::droneSpeeds speeds_reference_msg;

  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  nh.param<std::string>("position_reference", speeds_reference_str, "droneSpeedsRefs");

  speeds_reference_pub =
      nh.advertise<droneMsgsROS::droneSpeeds>('/' + drone_id_namespace + '/' + speeds_reference_str, 1);

  speeds_reference_msg.dx = 1;
  speeds_reference_msg.dy = 1;
  speeds_reference_msg.dz = 1;
  speeds_reference_msg.dyaw = 1;
  speeds_reference_msg.dpitch = 1;
  speeds_reference_msg.dyaw = 1;

  speeds_reference_adapter.setUp();
  speeds_reference_adapter.start();

  pL.start();

  ros::Rate rate(30);

  speeds_reference_adapter.run();

  speeds_reference_pub.publish(speeds_reference_msg);

  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();

  EXPECT_TRUE(pL.motion_reference_speeds_msgReceived);
}

/*--------------------------------------------*/
/*  Main  */
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;

  return RUN_ALL_TESTS();
}
