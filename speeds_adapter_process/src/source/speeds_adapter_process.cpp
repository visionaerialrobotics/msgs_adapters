/*!*******************************************************************************************
 *  \file       speeds_adapter_process.cpp
 *  \brief      SpeedsAdapter implementation file.
 *  \details    This file implements the SpeedsAdapter class.
 *  \authors    Abraham Carrera.
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/speeds_adapter_process.h"

SpeedsAdapter::SpeedsAdapter()
{
}

SpeedsAdapter::~SpeedsAdapter()
{
}

/*DroneProcess*/
void SpeedsAdapter::ownSetUp()
{
  ros::NodeHandle private_handle;

  private_handle.param<std::string>("drone_id", drone_id, "1");
  private_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  private_handle.param<std::string>("trajectoryControllerSpeedReferencesRebroadcast", trajectory_controller_speed_str,
                                    "trajectoryControllerSpeedReferencesRebroadcast");
  private_handle.param<std::string>("trajectoryControllerTwistReferencesRebroadcast",
                                    trajectory_controller_geometric_speed_str,
                                    "trajectoryControllerTwistReferencesRebroadcast");
  private_handle.param<std::string>("drone_speeds_refs", drone_speeds_ref_str, "droneSpeedsRefs");
  private_handle.param<std::string>("motion_reference_speeds", motion_reference_speeds_str, "motion_reference/speeds");
}

void SpeedsAdapter::ownStart()
{
  trajectory_controller_geometric_speed_pub = node_handle.advertise<geometry_msgs::TwistStamped>(
      '/' + drone_id_namespace + '/' + trajectory_controller_geometric_speed_str, 1);

  trajectory_controller_speed_sub =
      node_handle.subscribe('/' + drone_id_namespace + '/' + trajectory_controller_speed_str, 1,
                            &SpeedsAdapter::trajectoryControllerSpeedCallback, this);

  motion_reference_speeds_pub = node_handle.advertise<geometry_msgs::TwistStamped>(
      '/' + drone_id_namespace + '/' + motion_reference_speeds_str, 1);

  drone_speeds_ref_sub = node_handle.subscribe('/' + drone_id_namespace + '/' + drone_speeds_ref_str, 1,
                                               &SpeedsAdapter::speedsReferenceCallback, this);
}

void SpeedsAdapter::ownRun()
{
}

void SpeedsAdapter::ownStop()
{
  trajectory_controller_speed_sub.shutdown();
  trajectory_controller_speed_sub.shutdown();
  motion_reference_speeds_pub.shutdown();
  drone_speeds_ref_sub.shutdown();
}

/*Callbacks*/

void SpeedsAdapter::trajectoryControllerSpeedCallback(const droneMsgsROS::droneSpeeds& message)
{
  new_trajectory_controller_twist_msg.twist.linear.x = message.dx;
  new_trajectory_controller_twist_msg.twist.linear.y = message.dy;
  new_trajectory_controller_twist_msg.twist.linear.z = message.dz;
  new_trajectory_controller_twist_msg.twist.angular.x = message.droll;
  new_trajectory_controller_twist_msg.twist.angular.y = message.dpitch;
  new_trajectory_controller_twist_msg.twist.angular.z = message.dyaw;

  trajectory_controller_geometric_speed_pub.publish(new_trajectory_controller_twist_msg);
}

void SpeedsAdapter::speedsReferenceCallback(const droneMsgsROS::droneSpeeds& msg)
{
  motion_reference_speeds_msg.twist.linear.x = msg.dx;
  motion_reference_speeds_msg.twist.linear.y = msg.dy;
  motion_reference_speeds_msg.twist.linear.z = msg.dz;

  motion_reference_speeds_msg.twist.angular.x = msg.dyaw;
  motion_reference_speeds_msg.twist.angular.y = msg.dpitch;
  motion_reference_speeds_msg.twist.angular.z = msg.droll;

  motion_reference_speeds_pub.publish(motion_reference_speeds_msg);
}
