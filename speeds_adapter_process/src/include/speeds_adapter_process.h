/*!*********************************************************************************
 *  \file       speeds_adapter_process.h
 *  \brief      SpeedsAdapter definition file.
 *  \details    This file contains the SpeedsAdapter declaration.
 *              To obtain more information about it's definition consult
 *              the speeds_adapter_process.cpp file.
 *  \authors    Abraham Carrera.
 *  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
 *              All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef SPEEDS_ADAPTER_PROCESS_H
#define SPEEDS_ADAPTER_PROCESS_H

/*System*/
#include <string>
#include <ros/ros.h>

/*Messages*/
#include <droneMsgsROS/droneSpeeds.h>
/*Aerostack*/
#include <robot_process.h>
#include <tf/tf.h>
#include <geometry_msgs/TwistStamped.h>

/*Class definition*/
class SpeedsAdapter : public RobotProcess
{
public:
  SpeedsAdapter();
  ~SpeedsAdapter();

private:
  ros::NodeHandle node_handle;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string trajectory_controller_speed_str;
  std::string trajectory_controller_geometric_speed_str;

  std::string drone_speeds_ref_str;
  std::string motion_reference_speeds_str;

  geometry_msgs::TwistStamped new_trajectory_controller_twist_msg;
  geometry_msgs::TwistStamped motion_reference_speeds_msg;

  ros::Publisher trajectory_controller_geometric_speed_pub;
  ros::Subscriber trajectory_controller_speed_sub;

  ros::Publisher motion_reference_speeds_pub;
  ros::Subscriber drone_speeds_ref_sub;

private:
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();

  /*Callbacks*/
private:
  void trajectoryControllerSpeedCallback(const droneMsgsROS::droneSpeeds&);
  void speedsReferenceCallback(const droneMsgsROS::droneSpeeds&);
};

#endif
