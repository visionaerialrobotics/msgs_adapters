/*!*******************************************************************************************
 *  \file       control_mode_adapter_process.cpp
 *  \brief      ControlModeAdapter implementation file.
 *  \details    This file implements the ControlModeAdapter class.
 *  \authors    Abraham Carrera.
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/


#include "../include/control_mode_adapter_process.h"

ControlModeAdapter::ControlModeAdapter()
{
}

ControlModeAdapter::~ControlModeAdapter()
{
}

void ControlModeAdapter::ownSetUp()
{
  node_handle.param<std::string>("drone_id", drone_id, "1");
  node_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  node_handle.param<std::string>("drone_control_mode", drone_control_mode_str, "controlMode");
  node_handle.param<std::string>("motion_control_mode", motion_control_mode_str,
                                 "motion_reference/flight_motion_control_mode");
}

void ControlModeAdapter::ownStart()
{
  motion_control_mode_pub = node_handle.advertise<aerostack_msgs::FlightMotionControlMode>(
      '/' + drone_id_namespace + '/' + motion_control_mode_str, 1);

  drone_control_mode_sub = node_handle.subscribe('/' + drone_id_namespace + '/' + drone_control_mode_str, 1,
                                                 &ControlModeAdapter::controlModeCallback, this);
}

void ControlModeAdapter::ownRun()
{
}

void ControlModeAdapter::ownStop()
{
  motion_control_mode_pub.shutdown();
  drone_control_mode_sub.shutdown();
}

void ControlModeAdapter::controlModeCallback(const droneMsgsROS::droneTrajectoryControllerControlMode& msg)
{
  motion_control_mode_msg.header = msg.header;
  motion_control_mode_msg.command = msg.command;
  motion_control_mode_pub.publish(motion_control_mode_msg);
}
