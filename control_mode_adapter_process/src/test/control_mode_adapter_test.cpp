/******************************************************************
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include <gtest/gtest.h>
#include "../include/control_mode_adapter_process.h"
#include <cstdio>

#include <iostream>

/*Parameters*/
ros::NodeHandle* nh;
class ControlModeListener
{
public:
  ros::NodeHandle nh;
  std::string motion_control_mode_str;
  std::string drone_id;
  std::string drone_id_namespace;
  ros::Subscriber motion_control_mode_sub;

  bool motion_control_mode_msgReceived;

  ControlModeListener()
  {
    motion_control_mode_msgReceived = false;
  }

  void start()
  {
    nh.param<std::string>("drone_id", drone_id, "1");
    nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
    nh.param<std::string>("motion_control_mode", motion_control_mode_str,
                          "motion_reference/flight_motion_control_mode");
    motion_control_mode_sub = nh.subscribe('/' + drone_id_namespace + '/' + motion_control_mode_str, 1,
                                           &ControlModeListener::motionControlModeCallback, this);
  }

  void motionControlModeCallback(const aerostack_msgs::FlightMotionControlMode& message)
  {
    motion_control_mode_msgReceived = true;
  }
};

TEST(AdaptersTests, controlModeTests)
{
  ros::NodeHandle nh;
  ControlModeListener pL;
  ControlModeAdapter control_mode_adapter;

  std::string control_mode_str;
  std::string drone_id;
  std::string drone_id_namespace;

  ros::Publisher control_mode_pub;

  droneMsgsROS::droneTrajectoryControllerControlMode control_mode_msg;

  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  nh.param<std::string>("position_reference", control_mode_str, "controlMode");

  control_mode_pub = nh.advertise<droneMsgsROS::droneTrajectoryControllerControlMode>(
      '/' + drone_id_namespace + '/' + control_mode_str, 1);

  control_mode_msg.command = 1;

  control_mode_adapter.setUp();
  control_mode_adapter.start();

  pL.start();

  ros::Rate rate(30);

  control_mode_adapter.run();

  control_mode_pub.publish(control_mode_msg);

  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();

  EXPECT_TRUE(pL.motion_control_mode_msgReceived);
}

/*--------------------------------------------*/
/*  Main  */
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;

  return RUN_ALL_TESTS();
}
