/*!*********************************************************************************
 *  \file       control_mode_adapter_process.h
 *  \brief      PoseAdapter definition file.
 *  \details    This file contains the ControlModeAdapter declaration.
 *              To obtain more information about it's definition consult
 *              the control_mode_adapter_process.cpp file.
 *  \authors    Abraham Carrera, Daniel del Olmo.
 *  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
 *              All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef CONTROL_MODE_ADAPTER_PROCESS_H
#define CONTROL_MODE_ADAPTER_PROCESS_H

/*System*/
#include <string>
#include <ros/ros.h>

/*Messages*/
#include <droneMsgsROS/droneTrajectoryControllerControlMode.h>

/*Aerostack*/
#include <robot_process.h>
#include <aerostack_msgs/FlightMotionControlMode.h>

/*Class definition*/
class ControlModeAdapter : public RobotProcess
{
public:
  ControlModeAdapter();
  ~ControlModeAdapter();

private:
  ros::NodeHandle node_handle;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string drone_control_mode_str;
  std::string motion_control_mode_str;

  ros::Publisher motion_control_mode_pub;
  ros::Subscriber drone_control_mode_sub;

  aerostack_msgs::FlightMotionControlMode motion_control_mode_msg;

  void controlModeCallback(const droneMsgsROS::droneTrajectoryControllerControlMode&);

private:
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
};

#endif
