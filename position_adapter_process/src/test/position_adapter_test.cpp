/*****************************************************************************
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include <gtest/gtest.h>
#include "../include/position_adapter_process.h"
#include <cstdio>

#include <iostream>

/*Parameters*/
ros::NodeHandle* nh;
class PositionListener
{
public:
  ros::NodeHandle nh;
  std::string motion_reference_position_str;
  std::string drone_id;
  std::string drone_id_namespace;
  ros::Subscriber motion_reference_position_sub;

  bool motion_reference_position_msgReceived;

  PositionListener()
  {
    motion_reference_position_msgReceived = false;
  }

  void start()
  {
    nh.param<std::string>("drone_id", drone_id, "1");
    nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
    nh.param<std::string>("motion_reference_position", motion_reference_position_str, "motion_reference/position");
    motion_reference_position_sub = nh.subscribe('/' + drone_id_namespace + '/' + motion_reference_position_str, 1,
                                                 &PositionListener::motionReferencePositionCallback, this);
  }

  void motionReferencePositionCallback(const geometry_msgs::PointStamped& message)
  {
    motion_reference_position_msgReceived = true;
  }
};

TEST(AdaptersTests, positionRefAdapterTests)
{
  ros::NodeHandle nh;
  PositionListener pL;
  PositionAdapter position_reference_adapter;

  std::string position_reference_str;
  std::string drone_id;
  std::string drone_id_namespace;

  ros::Publisher position_reference_pub;

  droneMsgsROS::dronePositionRefCommandStamped position_reference_msg;

  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  nh.param<std::string>("position_reference", position_reference_str, "dronePositionRefs");

  position_reference_pub = nh.advertise<droneMsgsROS::dronePositionRefCommandStamped>(
      '/' + drone_id_namespace + '/' + position_reference_str, 1);
  std::cout << '/' + drone_id_namespace + '/' + position_reference_str << std::endl;
  position_reference_msg.position_command.x = 1;
  position_reference_msg.position_command.y = 1;
  position_reference_msg.position_command.z = 1;

  position_reference_adapter.setUp();
  position_reference_adapter.start();

  pL.start();

  ros::Rate rate(30);

  position_reference_adapter.run();

  position_reference_pub.publish(position_reference_msg);

  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();

  EXPECT_TRUE(pL.motion_reference_position_msgReceived);
}

/*--------------------------------------------*/
/*  Main  */
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;

  return RUN_ALL_TESTS();
}
