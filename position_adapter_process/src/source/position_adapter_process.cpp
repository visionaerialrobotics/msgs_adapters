/*!*******************************************************************************************
 *  \file       position_adapter_process.cpp
 *  \brief      PositionAdapter implementation file.
 *  \details    This file implements the PositionAdapter class.
 *  \authors    Abraham Carrera.
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/position_adapter_process.h"

PositionAdapter::PositionAdapter()
{
}

PositionAdapter::~PositionAdapter()
{
}

void PositionAdapter::ownSetUp()
{
  node_handle.param<std::string>("drone_id", drone_id, "1");
  node_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  node_handle.param<std::string>("position_reference", position_reference_str, "dronePositionRefs");
  node_handle.param<std::string>("motion_reference_position", motion_reference_position_str,
                                 "motion_reference/position");
}

void PositionAdapter::ownStart()
{
  motion_reference_position_pub = node_handle.advertise<geometry_msgs::PointStamped>(
      '/' + drone_id_namespace + '/' + motion_reference_position_str, 1);

  position_reference_sub = node_handle.subscribe('/' + drone_id_namespace + '/' + position_reference_str, 1,
                                                 &PositionAdapter::positionReferenceCallback, this);
}

void PositionAdapter::ownRun()
{
}

void PositionAdapter::ownStop()
{
  motion_reference_position_pub.shutdown();
  position_reference_sub.shutdown();
}

void PositionAdapter::positionReferenceCallback(const droneMsgsROS::dronePositionRefCommandStamped& msg)
{
  motion_reference_position_msg.header = msg.header;
  motion_reference_position_msg.point.x = msg.position_command.x;
  motion_reference_position_msg.point.y = msg.position_command.y;
  motion_reference_position_msg.point.z = msg.position_command.z;

  motion_reference_position_pub.publish(motion_reference_position_msg);
}
