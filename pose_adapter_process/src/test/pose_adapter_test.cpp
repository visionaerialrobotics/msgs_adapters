/******************************************************************************
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include <gtest/gtest.h>
#include "../include/pose_adapter_process.h"
#include <cstdio>

#include <iostream>

/*Parameters*/
ros::NodeHandle* nh;
class PoseListener
{
public:
  ros::NodeHandle nh;
  ros::Subscriber trajectory_controller_geometric_pose_sub;

  bool trajectory_controller_messageReceived;

  PoseListener()
  {
    trajectory_controller_messageReceived = false;
  }

  void start()
  {
    trajectory_controller_geometric_pose_sub = nh.subscribe("/drone1/trajectoryControllerPoseReferencesRebroadcast", 1,
                                                            &PoseListener::trajectoryControllerPoseCallback, this);
  }

  void trajectoryControllerPoseCallback(const geometry_msgs::PoseStamped& message)
  {
    trajectory_controller_messageReceived = true;
  }
};

TEST(AdaptersTests, poseAdapterTests)
{
  ros::NodeHandle nh;
  PoseListener pL;
  PoseAdapter pose_adapter;

  std::string trajectory_controller_pose_str;
  std::string drone_id;
  std::string drone_id_namespace;

  ros::Publisher trajectory_controller_pose_pub;

  droneMsgsROS::dronePose pose_msg;

  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  nh.param<std::string>("trajectoryControllerPositionReferencesRebroadcast", trajectory_controller_pose_str,
                        "trajectoryControllerPositionReferencesRebroadcast");

  trajectory_controller_pose_pub =
      nh.advertise<droneMsgsROS::dronePose>('/' + drone_id_namespace + '/' + trajectory_controller_pose_str, 1);

  pose_msg.x = 1;
  pose_msg.y = 1;
  pose_msg.z = 1;
  pose_msg.roll = 1;
  pose_msg.pitch = 1;
  pose_msg.yaw = 1;

  pose_adapter.setUp();
  pose_adapter.start();

  pL.start();

  ros::Rate rate(30);

  pose_adapter.run();

  trajectory_controller_pose_pub.publish(pose_msg);

  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();

  EXPECT_TRUE(pL.trajectory_controller_messageReceived);
}

/*--------------------------------------------*/
/*  Main  */
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;

  return RUN_ALL_TESTS();
}
