/*!*********************************************************************************
 *  \file       pose_adapter_process.h
 *  \brief      PoseAdapter definition file.
 *  \details    This file contains the PoseAdapter declaration.
 *              To obtain more information about it's definition consult
 *              the pose_adapter_process.cpp file.
 *  \authors    Abraham Carrera, Daniel del Olmo.
 *  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
 *              All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef POSE_ADAPTER_PROCESS_H
#define POSE_ADAPTER_PROCESS_H

/*System*/
#include <string>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <angles/angles.h>

/*Messages*/
#include <droneMsgsROS/dronePose.h>
/*Aerostack*/
#include <robot_process.h>
#include <tf/tf.h>
#include <tf2/LinearMath/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>

/*Class definition*/
class PoseAdapter : public RobotProcess
{
public:
  PoseAdapter();
  ~PoseAdapter();

private:
  ros::NodeHandle node_handle;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string trajectory_controller_pose_str;
  std::string trajectory_controller_geometric_pose_str;
  ros::Publisher trajectory_controller_geometric_pose_pub;
  ros::Subscriber trajectory_controller_pose_sub;

  geometry_msgs::PoseStamped new_trajectory_controller_pose_msg;

  void trajectoryControllerPoseCallback(const droneMsgsROS::dronePose&);
  Eigen::Quaternionf toQuaternion(double yaw, double pitch, double roll);

private:
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
};

#endif
