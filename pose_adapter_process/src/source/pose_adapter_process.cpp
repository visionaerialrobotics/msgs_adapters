/*!*******************************************************************************************
 *  \file       pose_adapter_process.cpp
 *  \brief      PoseAdapter implementation file.
 *  \details    This file implements the PoseAdapter class.
 *  \authors    Abraham Carrera.
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/pose_adapter_process.h"

PoseAdapter::PoseAdapter()
{
}

PoseAdapter::~PoseAdapter()
{
}

void PoseAdapter::ownSetUp()
{
  ros::NodeHandle private_handle("~");

  private_handle.param<std::string>("drone_id", drone_id, "1");
  private_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  private_handle.param<std::string>("trajectoryControllerPoseReferencesRebroadcast",
                                    trajectory_controller_geometric_pose_str,
                                    "trajectoryControllerPoseReferencesRebroadcast");
  private_handle.param<std::string>("trajectoryControllerPositionReferencesRebroadcast", trajectory_controller_pose_str,
                                    "trajectoryControllerPositionReferencesRebroadcast");
}

void PoseAdapter::ownStart()
{
  trajectory_controller_geometric_pose_pub = node_handle.advertise<geometry_msgs::PoseStamped>(
      '/' + drone_id_namespace + '/' + trajectory_controller_geometric_pose_str, 1);

  trajectory_controller_pose_sub =
      node_handle.subscribe('/' + drone_id_namespace + '/' + trajectory_controller_pose_str, 1,
                            &PoseAdapter::trajectoryControllerPoseCallback, this);
}

void PoseAdapter::ownRun()
{
}

void PoseAdapter::ownStop()
{
  trajectory_controller_pose_sub.shutdown();
}

void PoseAdapter::trajectoryControllerPoseCallback(const droneMsgsROS::dronePose& msg)
{
  new_trajectory_controller_pose_msg.pose.position.x = msg.x;
  new_trajectory_controller_pose_msg.pose.position.y = msg.y;
  new_trajectory_controller_pose_msg.pose.position.z = msg.z;

  Eigen::Quaternionf q;
  q = toQuaternion(msg.yaw, msg.pitch, msg.roll);

  new_trajectory_controller_pose_msg.pose.orientation.x = q.x();
  new_trajectory_controller_pose_msg.pose.orientation.y = q.y();
  new_trajectory_controller_pose_msg.pose.orientation.z = q.z();
  new_trajectory_controller_pose_msg.pose.orientation.w = q.w();

  trajectory_controller_geometric_pose_pub.publish(new_trajectory_controller_pose_msg);
}

Eigen::Quaternionf PoseAdapter::toQuaternion(double yaw, double pitch, double roll)  // yaw (Z), pitch (Y), roll (X)
{
  double cy = cos(yaw * 0.5);
  double sy = sin(yaw * 0.5);
  double cp = cos(pitch * 0.5);
  double sp = sin(pitch * 0.5);
  double cr = cos(roll * 0.5);
  double sr = sin(roll * 0.5);

  Eigen::Quaternionf q;
  q.w() = cy * cp * cr + sy * sp * sr;
  q.x() = cy * cp * sr - sy * sp * cr;
  q.y() = sy * cp * sr + cy * sp * cr;
  q.z() = sy * cp * cr - cy * sp * sr;
  return q;
}
