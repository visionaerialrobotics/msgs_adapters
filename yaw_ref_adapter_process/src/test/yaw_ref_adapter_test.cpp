/*****************************************************************************
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include <gtest/gtest.h>
#include "../include/yaw_ref_adapter_process.h"
#include <cstdio>

#include <iostream>

/*Parameters*/
ros::NodeHandle* nh;
class YawRefListener
{
public:
  ros::NodeHandle nh;
  std::string motion_reference_yaw_str;
  std::string drone_id;
  std::string drone_id_namespace;
  ros::Subscriber motion_reference_yaw_sub;

  bool motion_reference_yaw_msgReceived;

  YawRefListener()
  {
    motion_reference_yaw_msgReceived = false;
  }

  void start()
  {
    nh.param<std::string>("drone_id", drone_id, "1");
    nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
    nh.param<std::string>("motion_reference_yaw", motion_reference_yaw_str, "motion_reference/yaw");
    motion_reference_yaw_sub = nh.subscribe('/' + drone_id_namespace + '/' + motion_reference_yaw_str, 1,
                                            &YawRefListener::motionReferenceYawCallback, this);
  }

  void motionReferenceYawCallback(const aerostack_msgs::Float32Stamped& message)
  {
    motion_reference_yaw_msgReceived = true;
  }
};

TEST(AdaptersTests, yawRefAdapterTests)
{
  ros::NodeHandle nh;
  YawRefListener pL;
  YawRefAdapter yaw_reference_adapter;

  std::string yaw_reference_str;
  std::string drone_id;
  std::string drone_id_namespace;

  ros::Publisher yaw_reference_pub;

  droneMsgsROS::droneYawRefCommand yaw_reference_msg;

  nh.param<std::string>("drone_id", drone_id, "1");
  nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  nh.param<std::string>("position_reference", yaw_reference_str, "droneYawRefs");

  yaw_reference_pub =
      nh.advertise<droneMsgsROS::droneYawRefCommand>('/' + drone_id_namespace + '/' + yaw_reference_str, 1);

  yaw_reference_msg.yaw = 1;

  yaw_reference_adapter.setUp();
  yaw_reference_adapter.start();

  pL.start();

  ros::Rate rate(30);

  yaw_reference_adapter.run();

  yaw_reference_pub.publish(yaw_reference_msg);

  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();

  EXPECT_TRUE(pL.motion_reference_yaw_msgReceived);
}

/*--------------------------------------------*/
/*  Main  */
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;

  return RUN_ALL_TESTS();
}
