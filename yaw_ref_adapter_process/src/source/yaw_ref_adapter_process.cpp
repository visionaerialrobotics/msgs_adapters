/*!*******************************************************************************************
 *  \file       yaw_ref_adapter_process.cpp
 *  \brief      YawRefAdapter implementation file.
 *  \details    This file implements the YawRefAdapter class.
 *  \authors    Daniel Del Olmo.
 *  \copyright  Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/yaw_ref_adapter_process.h"

YawRefAdapter::YawRefAdapter()
{
}

YawRefAdapter::~YawRefAdapter()
{
}

/*DroneProcess*/
void YawRefAdapter::ownSetUp()
{
  node_handle.param<std::string>("drone_id", drone_id, "1");
  node_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  node_handle.param<std::string>("drone_yaw_refs", drone_yaw_ref_str, "droneYawRefs");
  node_handle.param<std::string>("motion_reference_yaw", motion_reference_yaw_str, "motion_reference/yaw");
}

void YawRefAdapter::ownStart()
{
  motion_reference_yaw_pub = node_handle.advertise<aerostack_msgs::Float32Stamped>(
      '/' + drone_id_namespace + '/' + motion_reference_yaw_str, 1);

  drone_yaw_ref_sub = node_handle.subscribe('/' + drone_id_namespace + '/' + drone_yaw_ref_str, 1,
                                            &YawRefAdapter::yawReferenceCallback, this);
}

void YawRefAdapter::ownRun()
{
}

void YawRefAdapter::ownStop()
{
  motion_reference_yaw_pub.shutdown();
  drone_yaw_ref_sub.shutdown();
}

void YawRefAdapter::yawReferenceCallback(const droneMsgsROS::droneYawRefCommand& msg)
{
  motion_reference_yaw_msg.header = msg.header;
  motion_reference_yaw_msg.value = msg.yaw;

  motion_reference_yaw_pub.publish(motion_reference_yaw_msg);
}
